<?php
class Comments extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
        $this->load->library('form_validation');
		$this->load->model('comments_model');
	}

	public function index()
	{
		$data['comments'] = $this->comments_model->get_comments();
        $data['title'] = 'Comments archive';
    
        $this->load->view('comments/index', $data);

	}

    public function view($id)
    {
        $data['comment'] = $this->comments_model->get_comments($id);
        
    
        if (empty($data['comment']))
        {
            show_404();
        }

        $data['content'] = $data['comment']['content'];

        $this->load->view('templates/header', $data);
        $this->load->view('comments/view', $data);
        $this->load->view('templates/footer');
    }

    public function create()
    {
        $data['title'] = 'Create a news item';
    
        $this->form_validation->set_rules('content', 'content', 'required');
    
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('comments/create');
            $this->load->view('templates/footer');
    
        } else {

            $data['result']= $this->comments_model->set_comments();
            
            $this->load->view('templates/header', $data);
            $this->load->view('/comments/create_result', $data);
            $this->load->view('templates/footer');

        }
    }

	public function reply()
    {
		$reply= $this->comments_model->reply();
		if ($reply) {
			die("<div style=\"background-color:#EFEFEF;padding:2px;\"><small>".date("Y-m-d H:i:S")."</small> <br />".$this->input->post('content')."</div>");
		}
		die("0");
	}

	public function commentchild($id)
    {

		$childs = $this->comments_model->get_childs($id);
		$data['childs'] = $childs;
		$this->load->view('/comments/commentchild', $data);
	}

}