<?php

class User extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('form');
        $this->load->library('form_validation');
		$this->load->model('users_model');
		$this->load->library('pagination');
	}

	public function index($page=1)
	{
		$data['title'] = 'User List';
		$perpage = 5;

		if ($page > 1) {
			$offset = ($page*$perpage)-$perpage;
		} else {
			$offset = 0;
		}

		$data['perpage'] = $perpage;
		$data['offset'] = $offset;
		$data['page'] = $page;

		$users = $this->users_model->get_users(FALSE, $perpage, $offset);
		$count = $this->users_model->get_total();


		$data['users'] = $users;
		$data['count'] = $count;
		$this->load->view('/users/index', $data);
	}

	public function update() {
		$update = $this->users_model->update_user($this->input->post('userid'), $this->input->post('name'), $this->input->post('email'), $this->input->post('password'));
		if ($update) {
			die('1');
		}else {
			die('0');
		}
	}

	public function delete($id) {

		$update = $this->users_model->delete_user($id);
		if ($update) {
			die('1');
		}else {
			die('0');
		}
	}

	public function login()
	{

		$this->form_validation->set_rules('emailLogin', 'Email', 'required');
		$this->form_validation->set_rules('passwordLogin', 'Password', 'required');

		$data['title'] = 'User Login';

		if ($this->form_validation->run() === FALSE)
        {
			$this->load->view('/users/login');
		} else {
			$login = $this->users_model->validate();

			if($login) // jika data user benar
			{
				$data = $this->users_model->get_users($this->input->post('emailLogin'));

				$this->session->set_userdata($data);
				die('1');

			} else {

				die('0');

			}
		} 

	}

	public function logout()
	{
		$this->session->sess_destroy();
		die("1");
	}

	public function dashboard()
	{
		$data = $this->session->all_userdata();
		$this->load->view('/users/dashboard', $data);
	}

	public function create($group="admin")
	{
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('repassword', 'Password', 'required');

		if ($this->form_validation->run() === FALSE)
        {
			$data['group']=$group;
			$data['title'] = 'Add New User '.ucfirst($group);
			$this->load->view('/users/create', $data);
		} else {
			//create new user
			$save = $this->users_model->set_users();
			if($save) {
				die($this->input->post('email'));
			} else {
				die("0");
			}

		}

	}

	public function profile($email = FALSE )
	{

		if ($email == "me") {
			$email = $this->session->userdata('email');
		}

		$data = $this->users_model->get_users($email);

		if (isset($data) && !empty($data) ) {
			$this->load->view('/users/profile', $data);
		} else {
			die("Not Found!");
		}
	}

}
