<?php
class Product extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

        $this->load->library('session');
		$this->load->helper('form');
        $this->load->library('form_validation');

		$this->load->model('products_model');
	}

	public function index($page=1)
	{

		$data['title'] = 'Product List';
		$perpage = 5;

		if ($page > 1) {
			$offset = ($page*$perpage)-$perpage;
		} else {
			$offset = 0;
		}

		$data['perpage'] = $perpage;
		$data['offset'] = $offset;
		$data['page'] = $page;

		$products = $this->products_model->get_products(FALSE, $perpage, $offset);
		$count = $this->products_model->get_total();

		$data['products'] = $products;
		$data['count'] = $count;

		$data['products'] = $products;
		$this->load->view('/products/index', $data);
	}

	public function update() {
		$update = $this->products_model
					->update_product(
						$this->input->post('id'),
						$this->input->post('name'),
						$this->input->post('category'),
						$this->input->post('sub_category'),
						$this->input->post('price'),
						$this->input->post('description'),
						$this->input->post('image') );
		if ($update) {
			die('1');
		}else {
			die('0');
		}
	}

	public function delete($id) {

		$update = $this->products_model->delete_product($id);
		if ($update) {
			die('1');
		}else {
			die('0');
		}
	}


    public function create()
	{
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('price', 'Email', 'required');

		if ($this->form_validation->run() === FALSE)
        {
            $data['title'] = 'Create New Product';
			$this->load->view('/products/create', $data);

		} else {
			//create new user
			$save = $this->products_model->set_products();
			if($save) {
				die($this->input->post('email'));
			} else {
				die("0");
			}

		}

	}

	public function selectionList($row) {
		$data['products'] = $this->products_model->get_all_products();
		$data['row'] = $row;
		$this->load->view('/products/selectionList', $data);
	}

}