<?php
class Orders extends CI_Controller {

    function __construct() {
		parent::__construct();
		$this->load->model('orders_model');
		$this->load->library('pagination');
	}

    public function index() {
        $orders = $this->orders_model->get_orders();

        foreach($orders as $k => $order) {
 
            $orders[$k]['detail']= $this->orders_model->get_orders_detail($order['id']);
        }

        $data['orders'] = $orders;
        $this->load->view('orders/index', $data);
    }

    public function create() {
        $data['title'] = "Form Order";
        $this->load->view('templates/header', $data);
        $this->load->view('orders/create');
        $this->load->view('templates/footer');
    }

    public function save() {
        /*
        echo $_POST['name']."|";
        echo $_POST['telp']."|";
        echo $_POST['address']."|";
        echo $_POST['total']."|";
        */
        $save = $this->orders_model->set_orders();
        if($save) {
            die("".$save);
        } else {
            die('0');
        }
        
    }

    public function save_detail() {
        /*
        echo $_POST['order_id']."|";
        echo $_POST['product_id']."|";
        echo $_POST['product_name']."|";
        echo $_POST['product_price']."|";
        echo $_POST['unit']."|";
        echo $_POST['total']."|";
        */
        $save = $this->orders_model->set_orders_details();
        if($save) {
            die("".$save);
        } else {
            die('0');
        }
        
    }

}