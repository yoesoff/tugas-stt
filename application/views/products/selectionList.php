<h3>Product (<?php echo $row ?>)</h3>
<table class="table">
    <?php
    $tmpCat = "";
    $tmpSubCat = "";
    foreach ($products as $key=>$product){
        if ($tmpCat=="" || $tmpCat != $product['category']) {
            $tmpCat = $product['category'];
            ?>
                <tr>
                    <td>
                        <strong><?php echo $tmpCat; ?></strong>
                    </td>
                </tr>
            <?php
        }
        if ($tmpSubCat="" || $tmpSubCat!= $product['sub_category']){
            $tmpSubCat= $product['sub_category'];
            ?>
                <tr>
                    <td>
                        <strong style="color: #6F6F6F;"><?php echo $tmpSubCat; ?></strong>
                    </td>
                </tr>
            <?php
        }
    ?>
        <tr>
            <td style="padding-left: 10px">
                <a href="#" class="productNameFromList" price="<?php echo $product['price'] ?>" name="<?php echo $product['name'] ?>" product_id="<?php echo $product['id'] ?>" >-&nbsp;<?php echo ucfirst($product['name']) ?></a> Rp.<?php echo ucfirst($product['price']) ?>
            </td>
        </tr>
    <?php
    }
    ?>
</table>

<script type="text/javascript">
$( document ).ready(function() {
    $("a.productNameFromList").click(function(){
        var price = $(this).attr("price");
        var name = $(this).attr("name");
        var selectedProductID = $(this).attr('product_id');


        $("td#price<?php echo $row ?>").html(price);
        $("td#ItemName<?php echo $row ?>").html(name);

        $("td#subTotal<?php echo $row ?>").html(price); //total
        //set meta data
        $("td#subTotal<?php echo $row ?>").attr("product_id", selectedProductID);
        $("td#subTotal<?php echo $row ?>").attr("product_name", name);
        $("td#subTotal<?php echo $row ?>").attr("product_price",price);

        //also handle grand total from total
        
        var grandTotal = 0;
        var allTDTotal = document.getElementsByClassName('subTotal');

        for(var x=0;x<allTDTotal.length;x++) {
            grandTotal = grandTotal+parseFloat(allTDTotal[x].innerHTML);
        }

        //alert(grandTotal);

        $("div#grandTotal").html(grandTotal);

        //other info
        
        $("tr#item<?php echo $row ?>").attr('product_id', selectedProductID);

    });

});
</script>