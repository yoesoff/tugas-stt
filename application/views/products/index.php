<?php $this->load->helper('url'); ?>

<?php
    $totalPage = $count / $perpage;
    if (!is_integer($totalPage)) {
        $totalPage = (int) $totalPage;
        $totalPage = $totalPage+1;
    }
    //echo $totalPage;
?>

<h1 style="width: 100%;text-align: center"><?php echo $title ?></h1>
<hr />

<a href="#" class="dashboard">Dashboard</a>

<div  style="width: 100%;text-align: center">
    <ul class="pagination">
      <li ><a href="#" class="first_page">&laquo;</a></li>
      <?php for($x=1;$x<=$totalPage;$x++) { ?>
      <li <?php echo ($page==$x)? "class=\"active\"" : "" ?>  ><a href="#" class="paginator_link" page="<?php echo $x ?>"><?php echo $x ?></a></li>
      <?php } ?>
      <li><a href="#" class="last_page">&raquo;</a></li>
    </ul>
</div>

<table class="table table-striped">
    <tr>
        <th>No.</th>
        <th>Name</th>

        <th>Price</th>
        <th>Category</th>
        <th>Sub Category</th>
        <th>Description</th>

        <th>Image</th>
        
        <th>Created</th>

        <th></th>
    </tr>

<?php
    $no = 1;
    foreach($products as $product) {
?>
    <tr id="<?php echo md5($product['id']) ?>">
        <td><?php echo $no; ?></td>
        <td><strong><?php echo $product['name'] ?></strong></td>
        <td><a href="mailto:<?php echo $product['price'] ?>?Subject=Hello" target="_top"><?php echo $product['price'] ?></a></td>
        <td><?php echo $product['category'] ?></td>
        <td><?php echo $product['sub_category'] ?></td>
        <td><?php echo ucfirst($product['description']) ?></td>
        <td><?php echo $product['image'] ?></td>
        <td><?php echo $product['created'] ?></td>
        <td>
           <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
              Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#" class="this_product_update" data-toggle="modal" data-target="#myModalUpdate" productid="<?php echo $product['id'] ?>" mViewName="<?php echo $product['name'] ?>" mViewPrice="<?php echo $product['price'] ?>" mViewCategory="<?php echo $product['category'] ?>" mViewSubCat="<?php echo $product['sub_category'] ?>" mViewImage="<?php echo $product['image'] ?>" mViewDescription="<?php echo $product['description'] ?>">Update</a></li>
              <li><a href="#" class="this_product_view" data-toggle="modal" data-target="#myModalView" productid="<?php echo $product['id'] ?>" mViewName="<?php echo $product['name'] ?>" mViewPrice="<?php echo $product['price'] ?>" mViewCategory="<?php echo $product['category'] ?>" mViewSubCat="<?php echo $product['sub_category'] ?>" mViewImage="<?php echo $product['image'] ?>" mViewDescription="<?php echo $product['description'] ?>">View</a></li>
              <li class="divider"></li>
              <li><a href="#" class="this_product_delete" product_id="<?php echo $product['id'] ?>">Delete</a></li>
            </ul>
          </div>

        </td>
    </tr>
<?php
        $no++;
    }
?>
</table>

<div  style="width: 100%;text-align: center">
    <ul class="pagination">
      <li ><a href="#" class="first_page">&laquo;</a></li>
      <?php for($x=1;$x<=$totalPage;$x++) { ?>
      <li <?php echo ($page==$x)? "class=\"active\"" : "" ?>  ><a href="#" class="paginator_link" page="<?php echo $x ?>"><?php echo $x ?></a></li>
      <?php } ?>
      <li><a href="#" class="last_page">&raquo;</a></li>
    </ul>
</div>

<hr />


<script type="text/javascript">
$( document ).ready(function() {
    //set dashboard as current
    $.cookie('url_state', '/product/index/<?php echo $page ?>');

    $( "div#mainContainer" ).toggle();
    $( "div#mainContainer" ).toggle('slow');

    $( ".dashboard").click(function(){ //go back to dashboard
        $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
         $.get( "/dashboard", function( data ) {
            $( "div#mainContainer" ).html( data );
        });
        return false;
    });
    //paginator
    $( "a.paginator_link").click(function(){
        var paginator_link = this;
        $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
        $.get( "/product/index/"+$(paginator_link).attr('page'), function( data ) {
          $( "div#mainContainer" ).html( data );
        });
        return false;
    });

    //first_page
    $( "a.first_page").click(function(){
        var paginator_link = this;
        $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
        $.get( "/product/index/1", function( data ) {
          $( "div#mainContainer" ).html( data );
        });
        return false;
    });

    //last_page
    $( "a.last_page").click(function(){
        var paginator_link = this;
        $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
        $.get( "/product/index/<?php echo $totalPage ?>", function( data ) {
          $( "div#mainContainer" ).html( data );
        });
        return false;
    });

    $( "a.this_product_view").click(function(){
        var productid=$(this).attr('productid');
        var mViewName=$(this).attr('mViewName');
        var mViewPrice=$(this).attr('mViewPrice');
        var mViewCategory=$(this).attr('mViewCategory');
        var mViewSubCat=$(this).attr('mViewSubCat');
        var mViewImage=$(this).attr('mViewImage');
        var mViewDescription=$(this).attr('mViewDescription');

        $("td#mViewName").html(mViewName);
        $("td#mViewPrice").html(mViewPrice);
        $("td#mViewCategory").html(mViewCategory);
        $("td#mViewSubCat").html(mViewSubCat);
        $("td#mViewImage").html(mViewImage);
        $("td#mViewDescription").html(mViewDescription);
    });

    $( "a.this_product_update").click(function(){
        var productid=$(this).attr('productid');
        var mViewName=$(this).attr('mViewName');
        var mViewPrice=$(this).attr('mViewPrice');
        var mViewCategory=$(this).attr('mViewCategory');
        var mViewSubCat=$(this).attr('mViewSubCat');
        var mViewImage=$(this).attr('mViewImage');
        var mViewDescription=$(this).attr('mViewDescription');

        $("input#product_id").val(productid);
        $("input#name").val(mViewName);

        if (mViewCategory == "Minuman") { //hide minuman
            $("div#sub_cat_makanan").hide("slow");
        } else { //makanan
            $("div#sub_cat_makanan").show("slow");
            if (mViewSubCat!="") {
                $("select#sub_category").val(mViewSubCat);//not empty select it
            }
        }

        if (mViewCategory!="") {
            $("select#category").val(mViewCategory);
        }

        $("input#inputPrice").val(mViewPrice);
        $("textarea#inputDesc").val(mViewDescription);
        $("input#inputImage").val(mViewImage);
    });

    $( "a.this_product_delete").click(function(){
        var conf = confirm("Are you sure want to delete");
        if (conf) {
              $.get( "/product/delete/"+$(this).attr('product_id'), function( data ) {
                if (data == "0") {
                    alert("Cannot Delete User!");
                } else {
                    $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
                    $.get( "/product/index/<?php echo $page ?>", function( data ) {
                      $( "div#mainContainer" ).html( data );
                    });
                }
            });
        }
    });

});
</script>

<!-- Modal -->
<div class="modal fade" id="myModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Detail User</h4>
      </div>
      <div class="modal-body">
        <table class="table table-striped">
            <tr>
                <td>Name</td> <td>:</td> <td id="mViewName"></td>
            </tr>
            <tr>
                <td>Price</td> <td>:</td> <td id="mViewPrice"></td>
            </tr>
            <tr>
                <td>Category</td> <td>:</td> <td id="mViewCategory"></td>
            </tr>
            <tr>
                <td>Sub Category</td> <td>:</td> <td id="mViewSubCat"></td>
            </tr>
            <tr>
                <td>Image</td> <td>:</td> <td  id="mViewImage"></td>
            </tr>
            <tr>
                <td>Description</td> <td>:</td> <td  id="mViewDescription"></td>
            </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="myModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Update User</h4>
      </div>
      <div class="modal-body">
        <div id="updateInfo"></div>
        <!-- Form Update -->
        <form role="form" class="form-horizontal" method="POST" action="/product/update" id="formUpdateProduct">
        
            <div id="updateInfo"></div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="name" >Name</label>
              <div class="col-sm-10">
                <input type="hidden" id="product_id" name="id" />
                <input type="text" placeholder="Product's name" id="name" class="form-control" name="name" data-validate="validate(required)" />
              </div>
            </div>
        
            <div class="form-group">
              <label class="col-sm-2 control-label" for="category" >Category</label>
              <div class="col-sm-10">
                <select id="category" class="form-control" name="category" style="width: 30%">
                    <option value="Makanan" selected>Makanan</option>
                    <option value="Minuman">Minuman</option>
                </select>
              </div>
            </div>
        
            <div class="form-group" id="sub_cat_makanan">
              <label class="col-sm-2 control-label" for="sub_category" >Sub Category</label>
              <div class="col-sm-10">
                <select id="sub_category" class="form-control" name="sub_category" style="width: 30%">
                    <option value="umum">Umum</option>
                    <option value="Side Dish">Side Dish</option>
                    <option value="Seafood">Seafood</option>
                    <option value="Light Meal">Light Meal</option>
                </select>
              </div>
            </div>
        
            <div class="form-group">
              <label class="col-sm-2 control-label" for="inputPrice" >Price</label>
              <div class="col-sm-10">
                <input type="text" placeholder="Price" id="inputPrice" class="form-control numberOnly" name="price" data-validate="validate(required, number)" />
              </div>
                <script type="text/javascript">
                $(".numberOnly").keydown(function(event) {
                // Allow: backspace, delete, tab, escape, and enter
                    if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
                         // Allow: Ctrl+A
                        (event.keyCode == 65 && event.ctrlKey === true) || 
                         // Allow: home, end, left, right
                        (event.keyCode >= 35 && event.keyCode <= 39)) {
                             // let it happen, don't do anything
                             return;
                    } else {
                        // Ensure that it is a number and stop the keypress
                        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                            event.preventDefault(); 
                        }   
                    }
                });
                </script>
            </div>
        
            <div class="form-group">
              <label class="col-sm-2 control-label" for="inputDesc">Description</label>
              <div class="col-sm-10">
                <textarea placeholder="Description" id="inputDesc" class="form-control" name="description" data-validate="validate(required)" ></textarea>
              </div>
            </div>
        
            <div class="form-group"> 
            <label class="col-sm-2 control-label" for="inputImage">Image</label>
              <div class="col-sm-10">
                <input type="text" placeholder="Image Url" id="inputImage" class="form-control" name="image" />
              </div>
            </div>
        
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button class="btn btn-default" type="submit" id="saveProduct">Update Product</button>
              </div>
            </div>
        </form>
        <!-- /End form update -->

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
        <script type="text/javascript">
            $( document ).ready(function() {
                // bind to the form's submit event 
               $('form#formUpdateProduct').submit(function() {
       
                    var options = {
                        target:        'div#updateInfo',   // target element(s) to be updated with server response 
                        beforeSubmit:  showRequest,  // pre-submit callback 
                        success:       showResponse,  // post-submit callback 
                        timeout:   10000 
                    };

                    if ($('input#name').val()=="" || $('input#inputPrice').val()=="" || $('input#inputDesc').val()==""){
                       alert("Please Complete input form");
                       return false;
                    }
                   $(this).ajaxSubmit(options); 
                   return false; 
               });
            });

          // pre-submit callback 
            function showRequest(formData, jqForm, options) {
                return true;
            } 

            // post-submit callback 
            function showResponse(responseText, statusText, xhr, $form) {
                if (responseText == "0") {
                    alert('Cannot Update User, please check your input then try again!')
                } else {
                    //refresh
                    $('.modal-backdrop').remove();

                    $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
                    $.get( "/product/index/<?php echo $page ?>", function( data ) {
                      $( "div#mainContainer" ).html( data );
                    });
                }
            }

        </script>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
