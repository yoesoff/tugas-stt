<?php $this->load->helper('url'); ?>
<h1><?php echo $title ?></h1>
<hr />
<a href="#" class="dashboard">Dashboard</a>
<form role="form" class="form-horizontal" method="POST" action="/product/create" id="formCreateProduct">

    <div class="form-group">
      <label class="col-sm-2 control-label" for="name" >Name</label>
      <div class="col-sm-10">
        <input type="text" placeholder="Product's name" id="name" class="form-control" name="name" data-validate="validate(required)" />
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="category" >Category</label>
      <div class="col-sm-10">
        <select id="category" class="form-control" name="category" style="width: 30%">
            <option value="Makanan" selected>Makanan</option>
            <option value="Minuman">Minuman</option>
        </select>
      </div>
    </div>

    <div class="form-group" id="sub_cat_makanan">
      <label class="col-sm-2 control-label" for="sub_category" >Sub Category</label>
      <div class="col-sm-10">
        <select id="sub_category" class="form-control" name="sub_category" style="width: 30%">
            <option value="umum">Umum</option>
            <option value="Side Dish">Side Dish</option>
            <option value="Seafood">Seafood</option>
            <option value="Light Meal">Light Meal</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="inputPrice" >Price</label>
      <div class="col-sm-10">
        <input type="text" placeholder="Price" id="inputPrice" class="form-control numberOnly" name="price" data-validate="validate(required, number)" />
      </div>
        <script type="text/javascript">
        $(".numberOnly").keydown(function(event) {
        // Allow: backspace, delete, tab, escape, and enter
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
                 // Allow: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) || 
                 // Allow: home, end, left, right
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            } else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault(); 
                }   
            }
        });
        </script>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="inputDesc">Description</label>
      <div class="col-sm-10">
        <textarea placeholder="Description" id="inputDesc" class="form-control" name="description" data-validate="validate(required)" ></textarea>
      </div>
    </div>

    <div class="form-group">
    <label class="col-sm-2 control-label" for="inputImage">Image</label>
      <div class="col-sm-10">
        <input type="text" placeholder="Image Url" id="inputImage" class="form-control" name="image" />
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button class="btn btn-default" type="submit" id="saveProduct">Add New</button>
      </div>
    </div>
</form>

<hr />


<script type="text/javascript">
$( document ).ready(function() {

    //set dashboard as current
    $.cookie('url_state', '/product/create');

    $("select#category").change(function(){
        if($("select#category").val()=="Makanan") {
            $("div#sub_cat_makanan").show('slow');
        } else {
            $("div#sub_cat_makanan").hide('slow');
        }
    });

    $( "div#mainContainer" ).toggle();
    $( "div#mainContainer" ).toggle('slow');

    $( ".dashboard").click(function(){ //go back to dashboard
        $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
        $.get( "/dashboard", function( data ) {
            $( "div#mainContainer" ).html( data );
        });
        return false;
    });

    //save new user
    $( "button#saveProduct" ).click(function(){
        var options = {
            target:        'div#mainContainer',   // target element(s) to be updated with server response 
            beforeSubmit:  showRequest,  // pre-submit callback 
            success:       showResponse,  // post-submit callback 
            timeout:   10000 
        };

        // bind to the form's submit event 
        $('form#formCreateProduct').submit(function() {

        if ($('input#name').val()=="" || $('input#inputPrice').val()=="" || $('input#inputDesc').val()==""){
            alert("Please Complete input form");
            return false;
        }
            $(this).ajaxSubmit(options); 
            return false; 
        });
    });

});

// pre-submit callback 
function showRequest(formData, jqForm, options) {
    //$( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />");
    //var queryString = $.param(formData); 
    //alert('About to submit: \n\n' + queryString);
    return true;
} 

// post-submit callback 
function showResponse(responseText, statusText, xhr, $form) {

    if (responseText == "0") {
        alert('Cannot save Product, please check your input then try again!');
    } else {
      
        $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
        $.get( "/dashboard", function( data ) {
            $( "div#mainContainer" ).html( data );
        });
       
        alert('Product created');
    }
}

</script>