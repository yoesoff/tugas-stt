<div class="container" id="mainContainer"></div>

<script type="text/javascript">
$( document ).ready(function() {

    $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
    
    var url_state = $.cookie('url_state');

    if (url_state != undefined && url_state != "/login") {

        $.get( url_state, function( data ) {
            $( "div#mainContainer" ).html( data );
        });
        
    } else {

        $.cookie('url_state', '/login')
        $.get( "/login", function( data ) {
            $( "div#mainContainer" ).html( data );
        });

    }

});
</script>


