<html>
<head>
	<title><?php echo $title ?> - CodeIgniter 2 Tutorial</title>
    <?php $this->load->helper('url'); ?>
    <link rel="stylesheet" href="<?php echo base_url()."assets/bootstrap/css/bootstrap.min.css"; ?>">
    <link rel="stylesheet" href="<?php echo base_url()."assets/bootstrap/css/bootstrap-theme.min.css"; ?>">
    <link rel="stylesheet" href="<?php echo base_url()."assets/css/signin.css"; ?>">
	<link rel="stylesheet" href="<?php echo base_url()."assets/css/ketchup.css"; ?>">
    
    <script type="text/javascript" src="<?php echo base_url()."assets/js/jquery.js"; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url()."assets/bootstrap/js/bootstrap.min.js"; ?>"></script>
	<script type="text/javascript" src="<?php echo base_url()."assets/js/jquery.form.min.js"; ?>"></script>
	<script type="text/javascript" src="<?php echo base_url()."assets/js/jquery.cookie.js"; ?>"></script>
	<script type="text/javascript" src="<?php echo base_url()."assets/js/ketchup.js"; ?>"></script>



</head>
<body>
	
