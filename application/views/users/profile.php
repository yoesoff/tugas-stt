<?php $this->load->helper('url'); ?>
<h1 style="width: 100%;text-align: center">Profile</h1>

<table class="table table-striped">
    <tr>
        <td>Name</td> <td>:</td> <td><?php echo $name ?></td>
    </tr>
    <tr>
        <td>Email</td> <td>:</td> <td><?php echo $email ?></td>
    </tr>
    <tr>
        <td>Group</td> <td>:</td> <td><?php echo ucfirst($group)?></td>
    </tr>
    <tr>
        <td>Join Date</td> <td>:</td> <td><?php echo ucfirst($created)?></td>
    </tr>
</table>

<a href="#" class="dashboard">Dashboard</a>

<script type="text/javascript">
$( document ).ready(function() {
    //set dashboard as current
    $.cookie('url_state', '/profile/<?php echo $email ?>');
    
    $( "div#mainContainer" ).toggle();
    $( "div#mainContainer" ).toggle('slow');

    $( ".dashboard").click(function(){ //go back to dashboard
        $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
         $.get( "/dashboard", function( data ) {
            $( "div#mainContainer" ).html( data );
        });
        return false;
    })
    

});
</script>