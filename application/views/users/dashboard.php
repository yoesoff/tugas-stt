<?php $this->load->helper('url'); ?>
<div class="container">

  <!-- Static navbar -->
  <div role="navigation" class="navbar navbar-default">
    <div class="navbar-header">
      <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="#" class="navbar-brand"><strong>Order Manager</strong></a>
    </div>
    <div class="navbar-collapse collapse">

      <ul class="nav navbar-nav navbar-right">
        <li class="menu"><a href="#" class="orderList">Orders</a></li>
        <li class="menu"><a href="#" class="commentsList">Comments</a></li>

        <?php if ($group=='admin') { ?>
        <li class="dropdown menu">
          <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            Menu
            <b class="caret"></b>
          </a>
          <ul class="dropdown-menu">
            <li class="dropdown-header">Action</li>
            <li><a href="#" class="product_create_admin">Create Product</a></li>
            <li><a href="#" class="product_list">Product List</a></li>
          </ul>
        </li>
      <?php } ?>

        <li class="dropdown menu">
          <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <strong><?php echo $name ?>
            </strong>
            <b class="caret"></b>
          </a>
          <ul class="dropdown-menu">
            <li class="dropdown-header">Action</li>
            <li><a href="#" class="profile">Profile</a></li>
            <?php if ($group=='admin') { ?>
            <li><a href="#" class="user_create_admin">Create Admin</a></li>
            <li><a href="#" class="user_create_employee">Create Employee</a></li>
            <li><a href="#" class="user_list">Users List</a></li>
            <?php } ?>
            <li class="divider"></li>
            <li><a href="#" class="logout"><strong>Logout</strong></a></li>
          </ul>
        </li>

      </ul>
    </div><!--/.nav-collapse -->
  </div>

  <!-- Main component for a primary marketing message or call to action -->
  <div class="jumbotron">
    <div class="well" id="innerContainer">
      
    </div>
  </div>

</div> <!-- /container -->

<script type="text/javascript">
$( document ).ready(function() {

  $( "div#mainContainer" ).toggle();
  $( "div#mainContainer" ).toggle('slow');
  //set dashboard as current
  $.cookie('url_state', '/dashboard');

  $( ".logout" ).click(function() {

    var conf = confirm("Logout, are you sure ?");

    if (conf) {

        $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
        //process logout
        $.get( "/logout", function( data ) {
            if (data == "1") {
                $.removeCookie('the_cookie');
                //request login page
                $.get( "/login", function( data ) {
                    $( "div#mainContainer" ).html( data );
                });
            } else {
              alert('Logout failed!');
            }
        });

    }

    return false;
  });

  $( ".profile" ).click(function() {
    $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
    $.get( "/profile/me", function( data ) {
      $( "div#mainContainer" ).html( data );
    });
    return false;
  });

  //create admin
  $( ".user_create_admin" ).click(function() {
    $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
    $.get( "/user/create/admin", function( data ) {
      $( "div#mainContainer" ).html( data );
    });
    return false;
  });

  //create employee
  $( ".user_create_employee" ).click(function() {
    $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
    $.get( "/user/create/employee", function( data ) {
      $( "div#mainContainer" ).html( data );
    });
    return false;
  });

  //user list/user index
  $( ".user_list" ).click(function() {
    $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
    $.get( "/user/index/1", function( data ) {
      $( "div#mainContainer" ).html( data );
    });
    return false;
  });

  //product_create_admin
  $( ".product_create_admin" ).click(function() {
    $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
    $.get( "/product/create", function( data ) {
      $( "div#mainContainer" ).html( data );
    });
    return false;
  });

  //product_list
  $( ".product_list" ).click(function() {
    $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
    $.get( "/product/index/1", function( data ) {
      $( "div#mainContainer" ).html( data );
    });
    return false;
  });

  //comment_list
  $( ".commentsList" ).click(function() {
    $( "div#innerContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
    $.get( "/comments", function( data ) {
        $( "div#innerContainer" ).toggle();
        $( "div#innerContainer" ).toggle('slow');

        $( "div#innerContainer" ).html( data );
    });
    return false;
  });

  //order_list
  $( ".orderList" ).click(function() {
    $( "div#innerContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
    $.get( "/orders/index", function( data ) {
      $( "div#innerContainer" ).html( data );
    });
    return false;
  });

  //auto load order
  $( "div#innerContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
  $.get( "/orders/index", function( data ) {
      $( "div#innerContainer" ).html( data );
  });

});
</script>