<?php $this->load->helper('url'); ?>

<form id="formLogin" role="form" class="form-signin" action="/login" method="POST">

    <h2 class="form-signin-heading">
        Please sign in
    </h2>

    <input id="emailLogin" name="emailLogin" type="email" autofocus="" required="" placeholder="Email address" class="form-control" />
    <input id="passwordLogin" name="passwordLogin" type="password" required="" placeholder="Password" class="form-control" />

    <label class="checkbox">
      <input type="checkbox" value="remember-me"> Remember me
    </label>

    <button type="submit" class="btn btn-lg btn-primary btn-block" id="btnSignin">
        Login
    </button>

</form>

<script type="text/javascript">
$( document ).ready(function() {

    $.cookie('url_state', '/login');

    $( "div#mainContainer" ).toggle();
    $( "div#mainContainer" ).toggle('slow');

    $( "button#btnSignin" ).click(function() {

        var options = { 
            target:        'div#mainContainer',   // target element(s) to be updated with server response 
            beforeSubmit:  showRequest,  // pre-submit callback 
            success:       showResponse,  // post-submit callback 
            timeout:   10000 
        }; 
 
        // bind to the form's submit event 
        $('form#formLogin').submit(function() { 
            $(this).ajaxSubmit(options); 
            return false; 
        }); 

    });
});


// pre-submit callback 
function showRequest(formData, jqForm, options) {
    //$( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />");

    //var queryString = $.param(formData); 
    //alert('About to submit: \n\n' + queryString); 
 
    return true; 
} 

// post-submit callback 
function showResponse(responseText, statusText, xhr, $form)  { 
        
    if (responseText == "0") {
        //login fails
        //$( "div#mainContainer" ).html(responseText);
        $.get( "/login", function( data ) {
            $( "div#mainContainer" ).html( data );
        });
        alert('Login failed, please try again');
    } else {

        $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />");

        //login oke, go to dashboard
        $.get( "/dashboard", function( data ) {
            $( "div#mainContainer" ).html(data);
        });
    }
}

</script>
