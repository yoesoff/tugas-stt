<?php $this->load->helper('url'); ?>
<h1><?php echo $title ?></h1>
<hr />

<a href="#" class="dashboard">Dashboard</a>
<form role="form" class="form-horizontal" method="POST" action="/user/create/<?php echo $group ?>" id="formCreateUser">

    <input type="hidden" name="group" value="<?php echo $group ?>" />

    <div class="form-group">
      <label class="col-sm-2 control-label" for="name" >Name</label>
      <div class="col-sm-10">
        <input type="text" placeholder="User's name" id="name" class="form-control" name="name" data-validate="validate(required)" />
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="inputEmail3" >Email</label>
      <div class="col-sm-10">
        <input type="email" placeholder="Email" id="inputEmail3" class="form-control" name="email" data-validate="validate(required, email)" />
      </div>
    </div>

    <div class="form-group passwordbox" >
      <label class="col-sm-2 control-label" for="inputPassword3">Password</label>
      <div class="col-sm-10">
        <input type="password" placeholder="Password" id="inputPassword3" class="form-control" name="password" data-validate="validate(required)" />
      </div>
    </div>

    <div class="form-group passwordbox" >
      <label class="col-sm-2 control-label" for="inputPassword4">ReType Password</label>
      <div class="col-sm-10">
        <input type="password" placeholder="Password" id="inputPassword4" class="form-control" name="repassword" data-validate="validate(required)"/>
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button class="btn btn-default" type="submit" id="saveUser">Add New</button>
      </div>
    </div>
</form>

<hr />

<script type="text/javascript">
$( document ).ready(function() {
    //set dashboard as current
    $.cookie('url_state', '/user/create/<?php echo $group ?>');

    $('form#formCreateUser').ketchup();

    $( "div#mainContainer" ).toggle();
    $( "div#mainContainer" ).toggle('slow');

    $( ".dashboard").click(function(){ //go back to dashboard
        $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
         $.get( "/dashboard", function( data ) {
            $( "div#mainContainer" ).html( data );
        });
        return false;
    });

    //save new user
    $( "button#saveUser" ).click(function(){
        var options = {
            target:        'div#mainContainer',   // target element(s) to be updated with server response 
            beforeSubmit:  showRequest,  // pre-submit callback 
            success:       showResponse,  // post-submit callback 
            timeout:   10000 
        };

        // bind to the form's submit event 
        $('form#formCreateUser').submit(function() {
            //form empty
            if ($('input#name').val()=="" || $('input#inputEmail3').val()=="" || $('input#inputPassword3').val()=="" || $('input#inputPassword4').val()=="") {
                alert("Please Complete input form");
                return false;
            }
            //password not match
            if ($('input#inputPassword3').val()!=$('input#inputPassword4').val()) {
                alert("Password do not match!");
                $('div.passwordbox').css('background', 'red');
                return false;
            }

            $(this).ajaxSubmit(options); 
            return false; 
        });
    });

});

// pre-submit callback 
function showRequest(formData, jqForm, options) {
    //$( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />");
    //var queryString = $.param(formData); 
    //alert('About to submit: \n\n' + queryString);
    return true;
} 

// post-submit callback 
function showResponse(responseText, statusText, xhr, $form) {

    if (responseText == "0") {
        alert('Cannot save User, please check your input then try again!')
    } else {
        $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />");
        
        //created
        $.get( "/profile/"+responseText, function( data ) {
          $( "div#mainContainer" ).html( data );
        });

    }
}

</script>