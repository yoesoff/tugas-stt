<?php
    $totalPage = $count / $perpage;
    if (!is_integer($totalPage)) {
        $totalPage = (int) $totalPage;
        $totalPage = $totalPage+1;
    }
    //echo $totalPage;
?>

<?php $this->load->helper('url'); ?>
<h1 style="width: 100%;text-align: center"><?php echo $title ?></h1>
<hr />
<a href="#" class="dashboard">Dashboard</a>
<div  style="width: 100%;text-align: center">
    <ul class="pagination">
        <li><a href="#" class="first_page">&laquo;</a></li>
        <?php for($x=1;$x<=$totalPage;$x++) { ?>
        <li <?php echo ($page==$x)? "class=\"active\"" : "" ?> ><a href="#" class="paginator_link" page="<?php echo $x ?>"><?php echo $x ?></a></li>
        <?php } ?>
        <li><a href="#" class="last_page">&raquo;</a></li>
    </ul>
</div>

<table class="table table-striped">
    <tr>
        <th>No.</th>
        <th>Name</th>

        <th>Email</th>

        <th>Group</th>

        <th>Join Date</th>
        
        <th>Action</th>
    </tr>

<?php
    $no = 1;
    foreach($users as $user) {
?>
    <tr id="<?php echo md5($user['id']) ?>">
        <td><?php echo $no; ?></td>
        <td><strong><?php echo $user['name'] ?></strong></td>
        <td><a href="mailto:<?php echo $user['email'] ?>?Subject=Hello" target="_top"><?php echo $user['email'] ?></a></td>
        <td><?php echo ucfirst($user['group']) ?></td>
        <td><?php echo $user['created'] ?></td>
        <td>
           <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
              Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#" class="this_user_update" data-toggle="modal" data-target="#myModalUpdate"  userid="<?php echo $user['id'] ?>" username="<?php echo $user['name'] ?>" useremail="<?php echo $user['email'] ?>">Update</a></li>
              <li><a href="#" class="this_user_view" data-toggle="modal" data-target="#myModalView"  userid="<?php echo $user['id'] ?>" username="<?php echo $user['name'] ?>" useremail="<?php echo $user['email'] ?>" usergroup="<?php echo $user['group'] ?>" usercreated="<?php echo $user['created'] ?>">View</a></li>
              <li class="divider"></li>
              <li><a href="#" class="this_user_delete" userid="<?php echo $user['id'] ?>" username="<?php echo $user['name'] ?>" useremail="<?php echo $user['email'] ?>" >Delete</a></li>
            </ul>
          </div>

        </td>
    </tr>
<?php
        $no++;
    }
?>

</table>

<div  style="width: 100%;text-align: center">
    <ul class="pagination">
        <li><a href="#" class="first_page">&laquo;</a></li>
        <?php for($x=1;$x<=$totalPage;$x++) { ?>
        <li <?php echo ($page==$x)? "class=\"active\"" : "" ?> ><a href="#" class="paginator_link" page="<?php echo $x ?>"><?php echo $x ?></a></li>
        <?php } ?>
        <li><a href="#" class="last_page">&raquo;</a></li>
    </ul>
</div>

<hr />

<script type="text/javascript">
$( document ).ready(function() {
    //set dashboard as current
    $.cookie('url_state', '/user/index/<?php echo $page ?>');
    
    $('form#formUpdateUser').ketchup();

    $( "div#mainContainer" ).toggle();
    $( "div#mainContainer" ).toggle('slow');

    $( ".dashboard").click(function(){ //go back to dashboard
        $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
         $.get( "/dashboard", function( data ) {
            $( "div#mainContainer" ).html( data );
        });
        return false;
    });

    //paginator
    $( "a.paginator_link").click(function(){
        var paginator_link = this;
        $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
        $.get( "/user/index/"+$(paginator_link).attr('page'), function( data ) {
          $( "div#mainContainer" ).html( data );
        });
        return false;
    });

    //first_page
    $( "a.first_page").click(function(){
        var paginator_link = this;
        $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
        $.get( "/user/index/1", function( data ) {
          $( "div#mainContainer" ).html( data );
        });
        return false;
    });

    //last_page
    $( "a.last_page").click(function(){
        var paginator_link = this;
        $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
        $.get( "/user/index/<?php echo $totalPage ?>", function( data ) {
          $( "div#mainContainer" ).html( data );
        });
        return false;
    });
    // /paginator

    //when modal update closed
    $('#myModalUpdate').on('hidden.bs.modal', function (e) {
        $(".ketchup-error").hide('slow');
    });

    //when modal update closed
    $('a.this_user_update').click(function (e) {
        var userid = $(this).attr('userid');
        var username = $(this).attr('username');
        var useremail = $(this).attr('useremail');

        //include it to form
        $("#userid").val(userid);
        $("#name").val(username);
        $("#email").val(useremail);

        $("#pass1").val("");
        $("#pass2").val("");
    });
    //when modal update closed
    $('a.this_user_view').click(function (e) {

        var userid = $(this).attr('userid');
        var username = $(this).attr('username');
        var useremail = $(this).attr('useremail');
        var usergroup = $(this).attr('usergroup');
        var usercreated = $(this).attr('usercreated');

        $("td#mViewName").html(username);
        $("td#mViewEmail").html(useremail);
        $("td#mViewGroup").html(usergroup);
        $("td#mViewCreated").html(usercreated);
    });
    
    $('a.this_user_delete').click(function (e) {
        var conf = confirm("Are You Sure ?");
        if (conf) {
            $.get( "/user/delete/"+$(this).attr('userid'), function( data ) {
                if (data == "0") {
                    alert("Cannot Delete User!");
                } else {
                    $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
                    $.get( "/user/index/<?php echo $page ?>", function( data ) {
                      $( "div#mainContainer" ).html( data );
                    });
                }
            });
        }
        return false;
    });
});
</script>

<!-- Modal -->
<div class="modal fade" id="myModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Update User</h4>
      </div>
      <div class="modal-body">
        <div id="updateInfo"></div>
        <!-- Form Update -->
            <form role="form" class="form-horizontal" method="POST" action="/user/update" id="formUpdateUser">

                <input type="hidden" id="userid" name="userid" value="" />
            
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="name" >Name</label>
                  <div class="col-sm-10">
                    <input type="text" placeholder="User's name" id="name" class="form-control" name="name" data-validate="validate(required)"/>
                  </div>
                </div>
            
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="inputEmail3" >Email</label>
                  <div class="col-sm-10">
                    <input type="email" placeholder="Email" id="email" class="form-control" name="email" data-validate="validate(required,email)"/>
                  </div>
                </div>
                <h3>Change password or Leave Empty</h3>
                <div class="form-group" style="">
                  <label class="col-sm-2 control-label" for="inputPassword3">Password</label>
                  <div class="col-sm-10">
                    <input type="password" placeholder="Password" id="pass1" class="form-control passworduser" name="password" />
                  </div>
                </div>
            
                <div class="form-group" style="">
                  <label class="col-sm-2 control-label" for="inputPassword4">ReType Password</label>
                  <div class="col-sm-10">
                    <input type="password" placeholder="Password" id="pass2" class="form-control passworduser" name="repassword" />
                  </div>
                </div>
                <div style="width: 100%;text-align: center">
                    <button class="btn btn-warning" type="submit" id="updateUser">Update</button>
                </div>
            </form>
        <!-- /End form update -->

        <!-- javascript to handle it -->
            <script type="text/javascript">

            $( document ).ready(function() {

                //save new user
                $( "button#updateUser" ).click(function(){
                                    
                    var options = {
                        target:        'div#updateInfo',   // target element(s) to be updated with server response 
                        beforeSubmit:  showRequest,  // pre-submit callback 
                        success:       showResponse,  // post-submit callback 
                        timeout:   10000 
                    };
            
                    // bind to the form's submit event 
                    $('form#formUpdateUser').submit(function() {
                        $(this).ajaxSubmit(options); 
                        return false; 
                    });
                   

                });

            });

            // pre-submit callback 
            function showRequest(formData, jqForm, options) {
                //$( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />");
                //var queryString = $.param(formData); 
                //alert('About to submit: \n\n' + queryString);
                return true;
            } 
            
            // post-submit callback 
            function showResponse(responseText, statusText, xhr, $form) {
                if (responseText == "0") {
                    alert('Cannot Update User, please check your input then try again!')
                } else {
                    //refresh
                    $('.modal-backdrop').remove();

                    $( "div#mainContainer" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
                    $.get( "/user/index/<?php echo $page ?>", function( data ) {
                      $( "div#mainContainer" ).html( data );
                    });
                }
            }

            $("input.passworduser").keyup(function(){
                if ($("input#pass1").val() != $("input#pass2").val()) {
                    $("input.passworduser").attr("style", "background: red");
                } else {
                    $("input.passworduser").attr("style", "background: green");
                }
            });

            </script>

        <!-- end Javascript -->

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="myModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Detail User</h4>
      </div>
      <div class="modal-body">
        <table class="table table-striped">
            <tr>
                <td>Name</td> <td>:</td> <td id="mViewName"></td>
            </tr>
            <tr>
                <td>Email</td> <td>:</td> <td id="mViewEmail"></td>
            </tr>
            <tr>
                <td>Group</td> <td>:</td> <td id="mViewGroup"></td>
            </tr>
            <tr>
                <td>Join Date</td> <td>:</td> <td  id="mViewCreated"></td>
            </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<style type="text/css">
.ketchup-error {
    z-index: 1000000
}
</style>