<h3>Comments</h3>
<hr />
<?php
    foreach ($comments as $comment) {
?>
    <div id="well" style="background-color: #CEFFBF;padding: 10px">
        <div class="well"><?php echo $comment['content'] ?></div>
    
        <small>Created : <?php echo $comment['created'] ?> </small>

        <a href="#" class="linkReply" parentid="<?php echo $comment['id'] ?>" id="<?php echo md5($comment['id']) ?>" data-toggle="modal" data-target="#modalReplayComment" style="float: right">Reply</a>

        <div id="replyBox<?php echo $comment['id'] ?>" style="padding-left: 15px;clear: both"></div>
        <script type="text/javascript">
        $( document ).ready(function() {
            //created
            $.get( "/comments/commentchild/<?php echo $comment['id'] ?>", function( data ) {
              $('div#replyBox<?php echo $comment['id'] ?>').append("<br />"+data);
            });
        });
        </script>

    </div>
<hr />
<?php
    }
?>

<script type="text/javascript">
$( document ).ready(function() {
    //set id
    $("a.linkReply").click(function(){
        $("input#id").val($(this).attr("parentid"));
    });
});
</script>

<!-- Modal -->
<div class="modal fade" id="modalReplayComment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Reply Comment</h4>
      </div>
      <div class="modal-body">
        <form role="form" class="form-horizontal" method="POST" action="/comments/reply" id="formReplyComment">
            <input type="hidden" id="id" name="id" />
            <textarea id="comment_reply" style="width: 100%;height: 50%;" name="content"> </textarea>
            <div style="width: 100%;text-align: center">
                <input type="submit" value="Post reply!" class="btn btn-success"/>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
        <script type="text/javascript">
            $( document ).ready(function() {
                var options = {
                    //target:        'div#replyBox',   // target element(s) to be updated with server response 
                    beforeSubmit:  showRequest,  // pre-submit callback 
                    success:       showResponse,  // post-submit callback 
                    timeout:   10000 
                };

                // bind to the form's submit event 
                $('form#formReplyComment').submit(function() {
                    //form empty
                    if ($('input#name').val()=="" || $('textarea#comment_reply').val()=="") {
                        alert("Please Complete input form");
                        return false;
                    }
        
                    $(this).ajaxSubmit(options); 
                    return false; 
                });
            });

            // pre-submit callback 
            function showRequest(formData, jqForm, options) {
                return true;
            } 
            
            // post-submit callback 
            function showResponse(responseText, statusText, xhr, $form) {
                if (responseText=="0") {
                    alert("reply failed");
                } else {
                    $('div#replyBox'+$('input#id').val()).append("<br />"+responseText+"<hr />");
                    $('textarea#comment_reply').val("");
                    alert("replied");
                }
            }

        </script>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->