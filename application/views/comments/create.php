<div class="well" style="margin: 35px">
    <h3>Contact Us</h3>

    <?php echo validation_errors(); ?>

    <?php echo form_open('comments/create') ?>

        <div class="form-group">
            <label for="content">Comments</label>
            <textarea name="content" id="content" cols="100" rows="5"></textarea>
        </div>
        <div class="form-group" style="width: 100%;text-align: center">
            <input class="btn btn-success" type="submit" name="submit" value="Create news item" />
        </div>

    </form>
</div>

<div id="searchlist"></div>
<script type="text/javascript">
    $( "div#searchlist" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
    $.get( "/comments", function( data ) {
        $( "div#innerContainer" ).toggle();
        $( "div#innerContainer" ).toggle('slow');

        $( "div#searchlist" ).html( data );
    });
</script>
    