<?php
    foreach($orders as $order) {
?>
        <div class="well" style="margin: 10px;width: 100%">
            <div style="width: 100%;text-align: right">
                <small><?php echo $order['created'] ?></small>
            </div>
            <div style="width: 100%;text-align: left">
                <small>Customer : <?php echo $order['name'] ?></small><br />
                <small>Phone : <?php echo $order['telp'] ?></small> <br />
                <small>Address : <?php echo $order['address'] ?></small> <br />

                 <table class="table table-striped" id="tableItem">
                    <tr>
                        <th>No.</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Unit</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                    <?php
                        $x=0;
                        $grandTotal = 0;
                        foreach($order['detail'] as $detail) {
                    ?>
                    <tr>
                            <td><?php echo $x ?></td>
                            <td><?php echo $detail['product_name'] ?></td>
                            <td><?php echo $detail['product_price'] ?></td>
                            <td><?php echo $detail['unit'] ?></td>
                            <td><?php echo $detail['total'] ?></td>
                            <td>1</td>
                    </tr>
                    <?php
                            $grandTotal = $grandTotal + $detail['total'];
                            $x++;
                        }
                    ?>
                </table>
                
                <table class="table table-striped">
                    <tr>
                        <th colspan="3" style="text-align: right"><strong>Grand Total</strong></th>
                        <th style="text-align: right">Rp.</th>
                        <th style="text-align: center;font-size: larger"><div id="grandTotal"><?php echo $grandTotal; ?></div></th>
                        <th></th>
                    </tr>
                </table>
            </div>
        
        </div>
<?php
    }
?>