<div style="padding: 25px">
    <table>
        <tr>
            <td>Name</td><td>:</td><td><input id="name" name="name" value="Anonimous" placeholder="Your Name" /></td>
        </tr>
        <tr>
            <td>Telp.</td><td>:</td><td><input id="telp" name="telp" value="+62" placeholder="Your Phone Number"/></td>
        </tr>
        <tr>
            <td>Address.</td><td>:</td><td><textarea id="address" name="address" cols=50 rows=3 placeholder="Your address"></textarea></td>
        </tr>
    </table>
    <div style="width: 100%;text-align: right;"> <a href="#" id="addItemLink">Add Item </a> </div>
    <div class="well">

        <table class="table table-striped" id="tableItem">
            <tr>
                <th>No.</th>
                <th>Item</th>
                <th>Price</th>
                <th>Unit</th>
                <th>Total</th>
                <th></th>
            </tr>
            <tr id="item1" product_id="" >
                <td>1</td>
                <td id="ItemName1"><a href="#" class="selectProduct" rowid="1" data-toggle="modal" data-target="#myModalProductList">Select</a></td>
                <td id="price1">Price</td>
                <td><input id="unit1" class="unitTotal numberOnly" value="1" itemid="1"/></td>
                <td id="subTotal1" class="subTotal" product_id="" product_name="" product_price="" unit="1"></td>
                <td></td>
            </tr>
        </table>
    
        <table class="table table-striped">
            <tr>
                <th colspan="3" style="text-align: right"><strong>Grand Total</strong></th>
                <th style="text-align: right">Rp.</th>
                <th style="text-align: center;font-size: larger"><div id="grandTotal">0</div></th>
                <th></th>
            </tr>
        </table>

        <table class="table table-striped">
            <tr style="width: 100%">
                <td style="width: 100%;text-align: center">
                    <button class="btn btn-info" type="submit" id="submitOrder" >Order Now!</button>
                </td>
            </tr>
        </table>
    
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModalProductList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Select Product</h4>
      </div>
      <div class="modal-body">
            <div id="productSelection"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
$( document ).ready(function() {

    $.cookie("pos_item", null);

    $.cookie('pos_item', 1);

    $( "a#addItemLink").click(function(){
        var itemnow = $.cookie('pos_item');
        $.cookie('pos_item', 1+parseInt($.cookie('pos_item')) );
        var newNum = $.cookie('pos_item');
        $( "table#tableItem").append("<tr id=\"item"+newNum+"\" product_id=\"\"><td>"+newNum+"</td><td id=\"ItemName"+newNum+"\"><a href=\"#\" rowid=\""+newNum+"\" data-toggle=\"modal\" data-target=\"#myModalProductList\" class=\"selectProduct\">Select</a></td><td id=\"price"+newNum+"\">Price</td><td><input class=\"unitTotal numberOnly\" itemid=\""+newNum+"\" id=\"unit"+newNum+"\" value=\"1\" /></td><td id=\"subTotal"+newNum+"\" class=\"subTotal\" product_id=\"\" product_name=\"\" product_price=\"\" unit=\"1\"></td><td><a href=\"#\" rowid=\"item"+newNum+"\" id=\"deleteItemLine"+itemnow+"\" class=\"deleteItemRow\">x</a></td></tr>");

        $( "a#deleteItemLine"+itemnow+"").click(function(){
            var conf = confirm("Are You Sure ("+itemnow+") ?");
            if (conf) {
                $.cookie('pos_item', parseInt($.cookie('pos_item'))-1 );
                $("tr#item"+newNum).remove();
            }
        });

        //select product
        $("a.selectProduct").click(function(){
            
            $( "div#productSelection" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
            var rowID = $(this).attr('rowid');
            //alert(rowID);
            $.get( "/product/selectionList/"+rowID, function( data ) {
            $( "div#productSelection" ).html( data );
            });
        });

        //unit changes
        $("input.unitTotal").keyup(function(){
    
            var itemid = $(this).attr('itemid');
            var price = parseFloat($( "td#price"+itemid).html());
    
            //update sub total
            $( "td#subTotal"+itemid).html(  price * parseFloat( $(this).val() ) );

            $( "td#subTotal"+itemid).attr('unit', $(this).val() );//
    
            var grandTotal = 0;
            var allTDTotal = document.getElementsByClassName('subTotal');
            for(var x=0;x<allTDTotal.length;x++) {
                grandTotal = grandTotal+parseFloat(allTDTotal[x].innerHTML);
            }
    
            //alert(grandTotal);
    
            $("div#grandTotal").html(grandTotal);
        });

        $(".numberOnly").keydown(function(event) {
        // Allow: backspace, delete, tab, escape, and enter
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
                 // Allow: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) || 
                 // Allow: home, end, left, right
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            } else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault(); 
                }   
            }
        });
        
    });

    //select product
    $("a.selectProduct").click(function(){
        $( "div#productSelection" ).html("<img src='<?php echo base_url()."assets/img/loading.gif"; ?>' />"); //loading
        var rowID = $(this).attr('rowid');
        //alert(rowID);
        $.get( "/product/selectionList/"+rowID, function( data ) {
        $( "div#productSelection" ).html( data );
        });
    });
    
    //unit changes
    $("input.unitTotal").keyup(function(){

        var itemid = $(this).attr('itemid');
        var price = parseFloat($( "td#price"+itemid).html());

        //update sub total
        $( "td#subTotal"+itemid).html(  price * parseFloat( $(this).val() ) );

        $( "td#subTotal"+itemid).attr('unit', $(this).val() );//

        var grandTotal = 0;
        var allTDTotal = document.getElementsByClassName('subTotal');
        for(var x=0;x<allTDTotal.length;x++) {
            grandTotal = grandTotal+parseFloat(allTDTotal[x].innerHTML);
        }

        //alert(grandTotal);

        $("div#grandTotal").html(grandTotal);
    });

    
    $("button#submitOrder").click(function(){
        if ($("input#name").val() == "" || $("input#telp").val() == "" || $("textarea#address").val() == "") {
            alert("Please Complete Form!");
        } else {
            //save master
            $.post( "/orders/save", { name: $("input#name").val(), telp: $("input#telp").val(), address:$("textarea#address").val(), total: $("div#grandTotal").html() })
            .done(function( data ) {
                if (data=="0") {
                    alert('Save Mater failed');
                } else {
                    
                    var allTDTotal = document.getElementsByClassName('subTotal');
                    for(var x=0;x<allTDTotal.length;x++) {
                        $.post( "/orders/save_detail", {order_id: data, product_id: allTDTotal[x].getAttribute("product_id") , product_name: allTDTotal[x].getAttribute("product_name"), product_price: allTDTotal[x].getAttribute("product_price"), unit: allTDTotal[x].getAttribute("unit"), total: allTDTotal[x].innerHTML})
                        .done(function( data ) {
                            if (data=="0") {
                                alert('Save Detail failed');
                            } else {
                                //alert((x)+"|"+allTDTotal.length);
                                //no handling first
                                if ((x) == allTDTotal.length) {
                                    alert("Transaction saved");
                                    window.location.replace("/orders");
                                }
                            }
                        });
                    }

                }
            });
        }
    });

});

</script>

<script type="text/javascript">
    $(".numberOnly").keydown(function(event) {
    // Allow: backspace, delete, tab, escape, and enter
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        } else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });
</script>