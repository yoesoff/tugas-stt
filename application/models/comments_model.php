<?php
class Comments_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

    public function get_comments($id = FALSE)
    {
        if ($id === FALSE)
        {
            $query = $this->db->get('comments');
            return $query->result_array();
        }

        $query = $this->db->get_where('comments', array('id' => $id));
        return $query->row_array();
    }

    public function get_childs($parentId = FALSE)
    {
		$this->db->where('parent_id', $parentId);
        $query = $this->db->get('comments');
        return $query->result_array();
    }

    public function set_comments()
    {
        $data = array(
            'content' => $this->input->post('content'),
            'created' => date("Y-m-d H:i:s")
        );
    
        return $this->db->insert('comments', $data);
    }
	
	public function reply()
    {
        $data = array(
			'parent_id' => $this->input->post('id'),
            'content' => $this->input->post('content'),
            'created' => date("Y-m-d H:i:s")
        );
    
        return $this->db->insert('comments', $data);
    }

}
