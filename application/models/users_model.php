<?php
class Users_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

    public function get_users($email = FALSE, $num=5, $offset=0)
    {
        if ($email === FALSE)
        {

            $query = $this->db->get('users', $num, $offset);
            return $query->result_array();
        }

        $query = $this->db->get_where('users', array('email' => $email));
        return $query->row_array();
    }

    public function set_users()
    {
        $this->load->helper('url');

        $data = array(
            'email' => $this->input->post('email'),
			'password' => md5($this->input->post('password')),
			'name' => $this->input->post('name'),
			'group' => $this->input->post('group'),
            'created' => date("Y-m-d H:i:s")
        );

        return $this->db->insert('users', $data);

    }

	public function get_total() {
		return $this->db->count_all_results('users'); 
	}

	public function update_user($id, $name = "", $email = "", $password = "") {
		if ($password!="") {
			$data = array('name' => $name, 'email' => $email, 'password' => md5($password) );		
		} else {
			$data = array('name' => $name, 'email' => $email);
		}

		$this->db->where('id', $id);
		return $this->db->update('users', $data); 
	}

	public function delete_user($id) {
		$this->db->where('id', $id);
		return $this->db->delete('users'); 
	}

	function validate()
	{
		$this->db->where('email', $this->input->post('emailLogin'));
		$this->db->where('password', md5($this->input->post('passwordLogin')));
		$query = $this->db->get_where('users');

		if($query->num_rows == 1) {
			return true;
		}
		return false;
	}


}
