<?php
class Products_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

    public function get_products($id = FALSE, $num=5, $offset=0)
    {
        if ($id === FALSE)
        {

            $query = $this->db->get('products', $num, $offset);
            return $query->result_array();
        }

        $query = $this->db->get_where('products', array('id' => $id));
        return $query->row_array();
    }

	public function get_all_products() {
		$this->db->order_by("category", "sub_category", "name");
		$query = $this->db->get('products');
        return $query->result_array();
	}

	public function get_total() {
		return $this->db->count_all_results('products'); 
	}

	public function update_product($id, $name = "", $category = "", $subcategory= "", $price="", $desc="", $image="") {

		$data = array("name"=>$name , "category" => $category, "sub_category"=> ($category=="Minuman") ? "": $subcategory, "price"=>$price, "description"=>$desc, "image"=>$image);

		$this->db->where('id', $id);
		return $this->db->update('products', $data); 
	}

	public function delete_product($id) {
		$this->db->where('id', $id);
		return $this->db->delete('products'); 
	}

    public function set_products()
    {
        $this->load->helper('url');

        $data = array(
            'name' => $this->input->post('name'),
			'price' => $this->input->post('price'),
			'category' => $this->input->post('category'),
			'sub_category' => ($this->input->post('category')=="Minuman") ? "" : $this->input->post('sub_category'),
			'description' => $this->input->post('description'),
			'image' => $this->input->post('image'),
            'created' => date("Y-m-d H:i:s")
        );

        return $this->db->insert('products', $data);

    }

}