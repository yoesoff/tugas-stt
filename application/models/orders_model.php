<?php
class Orders_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
    
	}

    public function set_orders()
    {

        $data = array(
            'total' => $_POST['total'],
			'name' => $_POST['name'],
			'address' => $_POST['address'],
			'telp' => $_POST['telp'],
            'created' => date("Y-m-d H:i:s")
        );

        $this->db->insert('orders', $data);
        return $this->db->insert_id();
    }

    public function set_orders_details()
    {

        $data = array(
            'order_id' => $_POST['order_id'],
            'product_id' => $_POST['product_id'],
			'product_name' => $_POST['product_name'],
			'product_price' => $_POST['product_price'],
			'unit' => $_POST['unit'],
            'total' => $_POST['total']
        );

        $this->db->insert('order_details', $data);
        return $this->db->insert_id();
    }

    public function get_orders()
    {

        $query = $this->db->get('orders');
        return $query->result_array();
      
    }

    public function get_orders_detail($order_id)
    {
        $this->db->where('order_id', $order_id);
        $query = $this->db->get('order_details');
        return $query->result_array();
    }

}