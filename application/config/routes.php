<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "welcome";
$route['404_override'] = '';

$route['comments/create'] = '/comments/create';
$route['comments/view/(:any)'] = '/comments/view/$1';
$route['comments/child/(:any)'] = '/comments/child/$1';
$route['comments/reply'] = '/comments/reply';
$route['comments/commentchild/(:any)'] = '/comments/commentchild/$1';

$route['comments'] = '/comments';
//$route['(:any)'] = '/pages/view/$1';

$route['login'] = '/user/login';
$route['logout'] = '/user/logout';
$route['profile/(:any)'] = '/user/profile/$1';
$route['profile'] = '/user/profile';
$route['dashboard'] = '/user/dashboard';
$route['user/create/(:any)'] = '/user/create/$1';
$route['user/create'] = '/user/create';
$route['user/index'] = '/user/index';
$route['user/index/(:any)'] = '/user/index/$1';
$route['user/update'] = '/user/update';
$route['user/delete/(:any)'] = '/user/delete/$1';

$route['product/create'] = '/product/create';
$route['product/index/(:any)'] = '/product/index/$1';
$route['product/update'] = '/product/update';
$route['product/delete/(:any)'] = '/product/delete/$1';
$route['product/selectionList/(:any)'] = '/product/selectionList/$1';

$route['orders'] = '/orders/create';
$route['orders/'] = '/orders/create';
$route['orders/index'] = '/orders/index';

//main
$route['container'] = '/pages/view';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
