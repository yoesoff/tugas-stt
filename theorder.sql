-- phpMyAdmin SQL Dump
-- version 3.5.8.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 17, 2013 at 03:29 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `theorder`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `content` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `parent_id`, `user_id`, `content`, `created`) VALUES
(11, NULL, NULL, 'asdasda', '2013-12-11 23:22:23'),
(12, NULL, NULL, 'asdasd', '2013-12-11 23:25:18'),
(13, NULL, NULL, 'aserrrr', '2013-12-13 21:25:59'),
(14, NULL, NULL, ' ssss', '2013-12-16 03:19:18'),
(15, NULL, NULL, ' bbbbb', '2013-12-16 03:21:03'),
(16, NULL, NULL, ' ssss', '2013-12-16 03:21:15'),
(17, NULL, NULL, ' vvvvvv', '2013-12-16 03:22:02'),
(18, NULL, NULL, ' zzzzz', '2013-12-16 03:22:43'),
(19, NULL, NULL, ' xxxxx', '2013-12-16 03:23:01'),
(20, NULL, NULL, ' aaaaa', '2013-12-16 03:24:37'),
(21, NULL, NULL, ' aaaaa', '2013-12-16 03:24:41'),
(22, NULL, NULL, ' aaaaa', '2013-12-16 03:25:31'),
(23, NULL, NULL, ' aaaaa', '2013-12-16 03:25:53'),
(24, NULL, NULL, ' aaaaa', '2013-12-16 03:25:56'),
(25, NULL, NULL, ' aaaaa', '2013-12-16 03:25:59'),
(26, NULL, NULL, ' dddd', '2013-12-16 03:27:10'),
(27, NULL, NULL, ' dddddsfsdf', '2013-12-16 03:27:17'),
(33, 11, NULL, ' egegegege', '2013-12-16 18:02:55'),
(34, 12, NULL, ' wewewew', '2013-12-16 18:04:17'),
(35, 11, NULL, 'sssss', '2013-12-16 18:05:26'),
(36, 11, NULL, ' dasdaasd', '2013-12-16 18:07:12'),
(37, 11, NULL, 'asdasdas', '2013-12-16 18:07:14'),
(38, 11, NULL, 'asdasd', '2013-12-16 18:07:16'),
(39, 11, NULL, ' berasadsadasd asdasdas sadas', '2013-12-16 18:42:05'),
(40, 12, NULL, ' wiewieiwei', '2013-12-16 18:43:53'),
(41, 12, NULL, ' asAsad', '2013-12-16 18:45:16'),
(42, NULL, NULL, 'komenku', '2013-12-17 02:37:04'),
(43, NULL, NULL, 'sadasd', '2013-12-17 02:38:42'),
(44, NULL, NULL, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2013-12-17 02:42:42');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total` double NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `total`, `name`, `address`, `telp`, `created`) VALUES
(10, 22000, 'yusuf', 'dasfsdf sdfsdfs', '+6223423', '2013-12-17 02:19:09'),
(11, 36000, 'jane', 'dsfsdf sdfsdf sdfsdfsd', '+6245345', '2013-12-17 02:22:57'),
(12, 24000, 'dere', 'dfsdf dfgdfgfdg', '+62', '2013-12-17 02:24:22'),
(13, 24000, 'Anonimous1', 'sadasdasdas', '+62', '2013-12-17 02:25:54'),
(14, 12000, 'Anonimous', 'ssss', '+62', '2013-12-17 02:31:27'),
(15, 12000, 'kokom', 'asdasd', '+625434', '2013-12-17 02:31:40'),
(16, 36000, 'Anonimous', 'dfgdfgdf', '+62', '2013-12-17 02:32:09'),
(17, 22000, 'Anonimous9', 'adasdas', '+624564', '2013-12-17 02:43:10');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE IF NOT EXISTS `order_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_price` double NOT NULL,
  `unit` int(11) NOT NULL,
  `total` double NOT NULL,
  `order_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `product_id`, `product_name`, `product_price`, `unit`, `total`, `order_id`) VALUES
(1, 9, 'Nasi Ayam Goreng', 12000, 1, 12000, 10),
(2, 20, 'Kwetiau Goreng/ Kuah', 10000, 1, 10000, 10),
(3, 9, 'Nasi Ayam Goreng', 12000, 2, 24000, 11),
(4, 16, 'Nasi Ayam Rica-Rica', 12000, 1, 12000, 11),
(5, 10, 'Nasi Ayam Saos Pedas', 12000, 1, 12000, 12),
(6, 15, 'Nasi Bistik Ayam', 12000, 1, 12000, 12),
(7, 11, 'Nasi Ayam Kuluyuk', 12000, 1, 12000, 13),
(8, 16, 'Nasi Ayam Rica-Rica', 12000, 1, 12000, 13),
(9, 9, 'Nasi Ayam Goreng', 12000, 1, 12000, 14),
(10, 11, 'Nasi Ayam Kuluyuk', 12000, 1, 12000, 15),
(11, 11, 'Nasi Ayam Kuluyuk', 12000, 1, 12000, 16),
(12, 16, 'Nasi Ayam Rica-Rica', 12000, 1, 12000, 16),
(13, 9, 'Nasi Ayam Goreng', 12000, 1, 12000, 16),
(14, 19, 'Mie Kangkung Ayam', 10000, 1, 10000, 17),
(15, 32, 'Nasi Cumi Tepung Saos Pedas', 12000, 1, 12000, 17);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `sub_category` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `category`, `sub_category`, `price`, `description`, `image`, `created`) VALUES
(8, 'Nasi Ayam Penyet / Bakar', 'Makanan', 'umum', '12000', 'Nasi Ayam Penyet / Bakar 12000', '', '2013-12-16 02:29:47'),
(9, 'Nasi Ayam Goreng', 'Makanan', 'umum', '12000', 'Nasi Ayam Goreng 12000\n', '', '2013-12-16 02:30:07'),
(10, 'Nasi Ayam Saos Pedas', 'Makanan', 'umum', '12000', 'Nasi Ayam Saos Pedas 12000\n', '', '2013-12-16 02:30:28'),
(11, 'Nasi Ayam Kuluyuk', 'Makanan', 'umum', '12000', 'Nasi Ayam Kuluyuk 12000', '', '2013-12-16 02:30:51'),
(12, 'Nasi Sapo Tahu Ayam', 'Makanan', 'umum', '12000', 'Nasi Sapo Tahu Ayam 12000\n ', '', '2013-12-16 02:31:24'),
(13, 'Nasi Goreng Ayam', 'Makanan', 'umum', '12000', 'Nasi Goreng Ayam 12000\n ', '', '2013-12-16 02:31:45'),
(14, 'Nasi Muntahu Ayam', 'Makanan', 'umum', '12000', 'Nasi Muntahu Ayam 12000', '', '2013-12-16 02:32:02'),
(15, 'Nasi Bistik Ayam', 'Makanan', 'umum', '12000', 'Nasi Bistik Ayam 12000', '', '2013-12-16 02:32:28'),
(16, 'Nasi Ayam Rica-Rica', 'Makanan', 'umum', '12000', 'Nasi Ayam Rica-Rica 12000', '', '2013-12-16 02:32:45'),
(17, 'Nasi Kangkung Ayam', 'Makanan', 'umum', '10000', 'Nasi Kangkung Ayam\n', '', '2013-12-16 02:32:59'),
(18, 'Mie Goreng Ayam', 'Makanan', 'umum', '10000', 'Mie Goreng Ayam\n', '', '2013-12-16 02:33:13'),
(19, 'Mie Kangkung Ayam', 'Makanan', 'umum', '10000', 'Mie Kangkung Ayam\n', '', '2013-12-16 02:33:25'),
(20, 'Kwetiau Goreng/ Kuah', 'Makanan', 'Side Dish', '10000', 'Kwetiau Goreng/ Kuah\n', '', '2013-12-16 02:33:51'),
(21, 'Bihung Goreng', 'Makanan', 'Side Dish', '10000', 'Bihung Goreng', '', '2013-12-16 02:34:07'),
(22, 'Nasi Capcay goreng/kuah', 'Makanan', 'Side Dish', '10000', 'Nasi Capcay goreng/kuah\n', '', '2013-12-16 02:34:19'),
(23, 'Nasi Omelet', 'Makanan', 'Side Dish', '10000', 'Nasi Omelet\n', '', '2013-12-16 02:34:33'),
(24, 'Nasi Goreng Special/Panggang', 'Makanan', 'Side Dish', '10000', 'Nasi Goreng Special/Panggang\n', '', '2013-12-16 02:35:00'),
(25, 'Nasi Fuyunghai ', 'Makanan', 'Side Dish', '10000', 'Nasi Fuyunghai \n', '', '2013-12-16 02:35:17'),
(26, 'Nasi/Mie Goreng Seafood', 'Makanan', 'Seafood', '10000', '', '', '2013-12-16 02:35:35'),
(27, 'Nasi/Mie Kangkung Seafood', 'Makanan', 'Seafood', '10000', 'Nasi/Mie Kangkung Seafood\n', '', '2013-12-16 02:35:56'),
(28, 'Nasi Cumi Pedas', 'Makanan', 'Seafood', '12000', 'Nasi Cumi Pedas\n', '', '2013-12-16 02:36:13'),
(29, 'Nasi Sapo Tahu Seafood', 'Makanan', 'Seafood', '12000', 'Nasi Sapo Tahu Seafood\n', '', '2013-12-16 02:36:29'),
(30, 'Nasi Seafood Goreng', 'Makanan', 'Seafood', '12000', 'Nasi Seafood Goreng\n', '', '2013-12-16 02:36:45'),
(31, 'Nasi Udang Saos Pedas', 'Makanan', 'Seafood', '12000', 'Nasi Udang Saos Pedas\n', '', '2013-12-16 02:37:03'),
(32, 'Nasi Cumi Tepung Saos Pedas', 'Makanan', 'Seafood', '12000', 'Nasi Cumi Tepung Saos Pedas\n', '', '2013-12-16 02:37:23'),
(33, ' Ikan Bawal Goreng/Bakar', 'Makanan', 'Seafood', '16000', ' Ikan Bawal Goreng/Bakar\n', '', '2013-12-16 02:37:44'),
(34, 'Ikan Lele Goreng/Bakar', 'Makanan', 'Seafood', '12000', 'Ikan Lele Goreng/Bakar\n', '', '2013-12-16 02:37:58'),
(35, 'Chicken Crispy', 'Makanan', 'Light Meal', '7000', 'Chicken Crispy', '', '2013-12-16 02:42:53'),
(36, 'Chicken Sausage', 'Makanan', 'Light Meal', '7000', 'Chicken Sausage', '', '2013-12-16 02:43:14'),
(37, 'Chicken Katsu', 'Makanan', 'Light Meal', '8000', 'Chicken Katsu\n', '', '2013-12-16 02:43:30'),
(38, 'Chicken Tempura', 'Makanan', 'umum', '8500', 'Chicken Tempura\n', '', '2013-12-16 02:43:53'),
(39, 'Chicken Chesse Katsu', 'Makanan', 'Light Meal', '8500', 'Chicken Chesse Katsu\n', '', '2013-12-16 02:44:11'),
(40, 'Chicken Chesse Tempura', 'Makanan', 'Light Meal', '9000', 'Chicken Chesse Tempura\n', '', '2013-12-16 02:44:26'),
(41, 'Chicken Cordon Blue', 'Makanan', 'Light Meal', '9000', 'Chicken Cordon Blue\n', '', '2013-12-16 02:44:42'),
(42, 'Chicken Beef Edam', 'Makanan', 'umum', '9000', 'Chicken Beef Edam\n', '', '2013-12-16 02:44:56'),
(43, 'Chicken Stroganoff', 'Makanan', 'Light Meal', '9000', 'Chicken Stroganoff\n', '', '2013-12-16 02:45:10'),
(44, 'Ice/Hot Tea', 'Minuman', '', '2500', 'Ice/Hot Tea\n', '', '2013-12-16 02:45:51'),
(45, 'Mineral Water', 'Minuman', '', '3000', 'Mineral Water\n', '', '2013-12-16 02:47:38'),
(46, 'Oreo Milkshake', 'Minuman', '', '8000', 'Oreo Milkshake\n', '', '2013-12-16 02:47:56'),
(47, 'Tomato Juice', 'Minuman', '', '5000', 'Tomato Juice\n', '', '2013-12-16 02:48:13'),
(48, 'Guava Juice', 'Minuman', '', '5000', 'Guava Juice ', '', '2013-12-16 02:48:32'),
(49, 'Avocado Juice', 'Minuman', '', '6000', 'avocado', '', '2013-12-16 02:49:04'),
(50, 'Melon', 'Minuman', '', '5000', 'Melon\n', '', '2013-12-16 02:49:20'),
(51, 'Mangga', 'Minuman', '', '5000', 'Mangga\n', '', '2013-12-16 02:49:39'),
(52, 'apel', 'Minuman', '', '5000', 'apel\n', '', '2013-12-16 02:49:54'),
(53, 'Orange Juice', 'Minuman', '', '5000', 'Orange Juice\n', '', '2013-12-16 02:50:11'),
(54, 'Ice Lychee', 'Minuman', '', '6000', 'Ice Lychee\n', '', '2013-12-16 02:50:32'),
(55, 'Es Soda Gembira', 'Minuman', '', '8000', 'Es Soda Gembira\n', '', '2013-12-16 02:50:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `group` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `group`, `created`) VALUES
(1, 'admin@order.com', '21232f297a57a5a743894a0e4a801fc3', 'Admin Casheep', 'admin', '2013-12-13 00:00:00'),
(2, 'user1@user.com', '24c9e15e52afc47c225b757e7bee1f9d', 'user12', 'admin', '2013-12-14 15:50:15'),
(3, 'employee1@employee.com', '03a395eaf1edb673e0f99c7ca8eb156a', 'employee1', 'employee', '2013-12-14 16:33:53'),
(4, 'hehehe@hhh.com', 'b2ca678b4c936f905fb82f2733f5297f', 'hehehe', 'employee', '2013-12-14 16:35:34'),
(8, 'waka@waka.com', '7187f8a707b74d0aceeff15769393052', 'waka waka', 'employee', '2013-12-14 16:42:48'),
(11, 'admin2@admin.com', 'c84258e9c39059a89ab77d846ddab909', 'admin2', 'admin', '2013-12-14 16:45:33'),
(12, 'emplosatu@hahah.com', '47bce5c74f589f4867dbd57e9ca9f808', 'emplosatu', 'employee', '2013-12-14 18:05:50'),
(13, 'employeehahah@gmail.com', '77963b7a931377ad4ab5ad6a9cd718aa', 'employeehahah', 'employee', '2013-12-14 18:06:30'),
(15, 'kokom@kokom.com', 'f06a6b3dd6053b62beaa9d49668bae45', 'kokom', 'employee', '2013-12-14 18:26:16'),
(16, 'xxxx@xxxx.com', 'ea416ed0759d46a8de58f63a59077499', 'xxxx', 'employee', '2013-12-15 15:20:50'),
(17, 'jonjon@yahoo.com', '74b87337454200d4d33f80c4663dc5e5', 'Jonjon setyawan', 'employee', '2013-12-16 00:13:44'),
(18, 'passwordbox@passwordbox.cc', '85fb3cf1fb5ae2a8e7c8f2941fb8479c', 'passwordbox', 'employee', '2013-12-16 00:19:08'),
(19, 'passwordbox@passwordbox.cc', '85fb3cf1fb5ae2a8e7c8f2941fb8479c', 'passwordbox', 'employee', '2013-12-16 00:19:08');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
